var app = angular.module('PlanThouLife', ['ngRoute', 'ngCookies', 'mp.datePicker', 'mp.colorPicker']);

app.config(function($routeProvider) {
    $routeProvider
        .when('/home', {
            controller: 'HomeController',
            templateUrl: 'views/home.html'
        })

    //   .when('/create_user', {
    //     controller: 'LoginController',
    //     templateUrl: 'views/new_user.html'
    //   })

    //   .when('/chat', {
    //     controller: 'ChatController',
    //     templateUrl: '/views/chat.html'
    //   })

    .otherwise({
        redirectTo: '/home'
    });
});